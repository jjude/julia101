# Learning Julia

This uses docker container with IJulia from https://jupyter-docker-stacks.readthedocs.io/en/latest/using/running.html.

To bring up the container run 

```
docker run --rm -p 8888:8888 -e JUPYTER_ENABLE_LAB=yes -v "$PWD":/home/jovyan/work jupyter/datascience-notebook
```

(or execute book.sh)

